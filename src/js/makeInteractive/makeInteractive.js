export default function makeInteractive(el, options) {
  console.log('makeInteractive');
  const desktopMQ = window.matchMedia('(min-width: 768px)');
  let zoomable = options.zoomable,
    zoomByScroll = options.zoomByScroll,
    draggable = options.draggable,
    bound = options.bound,
    defaultPositionX = options.defaultPosition[0],
    defaultPositionY = options.defaultPosition[1];

  let element, elWidth, elHeight, boundEl, boundWidth, boundHeight;

  let newX, newY;

  let mobileDefaultPositionX, mobileDefaultPositionY;

  if (!options.mobileDefaultPosition) {
    mobileDefaultPositionX = 50;
    mobileDefaultPositionY = 50;
  } else {
    mobileDefaultPositionX = options.mobileDefaultPosition[0];
    mobileDefaultPositionY = options.mobileDefaultPosition[1];
  }

  const zoomOut = document.getElementsByClassName('js-map-zoom-minus')[0],
    zoomIn = document.getElementsByClassName('js-map-zoom-plus')[0];


  let defaultZoom = options.defaultZoom,
    maxZoom = options.maxZoom,
    zoomStep = options.zoomStep,
    mobileZoom = options.mobileZoom,
    currentZoom;

  if (!defaultZoom) defaultZoom = 1;
  if (!maxZoom) maxZoom = 2;
  if (!zoomStep) zoomStep = .1;
  if (!mobileZoom) mobileZoom = 1;


  if (desktopMQ.matches) {
    currentZoom = defaultZoom;
  } else {
    currentZoom = mobileZoom;
  }

  console.log(el, zoomable, defaultZoom, zoomByScroll, draggable, bound, defaultPositionX, defaultPositionY)

  // checkElement

  if (el.indexOf('#') > -1) {
    element = document.getElementById(el.replace('#', ''));
    setElementSize();
  } else if (el.indexOf('.') > -1) {
    element = document.getElementsByClassName(el.replace('.', ''))[0];
    setElementSize();
  } else {
    console.log('makeInteractive: Undefined element!');
    return;
  }

  // checkAttention

  let mobileAttention, attention;

  if (options.mobileAttention) {
    mobileAttention = options.mobileAttention;

    if (mobileAttention.indexOf('#') > -1) {
      attention = document.getElementById(mobileAttention.replace('#', ''))
    } else if (el.indexOf('.') > -1) {
      attention = document.getElementsByClassName(mobileAttention.replace('.', ''))[0];
    } else {
      console.log('makeInteractive: Undefined attention element!');
      return;
    }
  }

  const style = window.getComputedStyle(element, null);

  // checkBoundElem

  if (bound) {
    if (bound.indexOf('#') > -1) {
      boundEl = document.getElementById(bound.replace('#', ''));
      setBoundSize();
    } else if (bound.indexOf('.') > -1) {
      boundEl = document.getElementsByClassName(bound.replace('.', ''))[0];
      setBoundSize();
    } else {
      boundEl = window;
      console.log('makeInteractive: Undefined bound!');
      return;
    }
  }

  zoomIn.onclick = function () {
    if (desktopMQ.matches) {
      zoomInByButtonDesktop()
    } else {
      zoomInByButtonMobile()
    }
  }

  zoomOut.onclick = function () {
    if (desktopMQ.matches) {
      zoomOutByButtonDesktop()
    } else {
      zoomOutByButtonMobile()
    }
  }
  makeDraggable();

  function setElementSize() {
    elWidth = element.offsetWidth;
    elHeight = element.offsetHeight;
  }

  function setBoundSize() {
    boundWidth = boundEl.offsetWidth;
    boundHeight = boundEl.offsetHeight;
  }

  function makeDraggable() {
    let st = element.style,
      startPos,
      startSecPos,
      left, top,
      draggable = false;

    st.position = 'absolute';
    st.top = '0';
    st.left = '0';
    st.transformOrigin = '0 0';


    if (desktopMQ.matches) {
      st.transform = 'translate3d(0, 0, 0)';
      element.onmousedown = function (event) {
        draggable = true;
        startPos = [];
        startPos.push(event.clientX);
        startPos.push(event.clientY);

        left = Number(style.getPropertyValue('transform').split(' ')[4].replace(',', ''));
        top = Number(style.getPropertyValue('transform').split(' ')[5].replace(')', ''));
        listenMove();
      };

      window.onmouseup = function (event) {
        draggable = false;
      }

      function listenMove() {
        window.onmousemove = function (event) {
          if (draggable === true) {
            let diffX = startPos[0] - event.clientX,
              diffY = startPos[1] - event.clientY;

            newX = left - diffX;
            newY = top - diffY;

            checkBound();
            element.style.transform = 'translate3d(' + newX + 'px, ' + newY + 'px, 0) scale(' + currentZoom + ')';
          }
        }
      }
    } else {
      st.transform = 'translate3d(' + -mobileDefaultPositionX + '%, ' + -mobileDefaultPositionY + '%, 0) scale(' + mobileZoom + ')';
      const delta = 30;

      element.addEventListener('touchstart', function (e) {
        if (e.touches.length > 1) {
          e.preventDefault();
          draggable = true;
          startPos = [];
          startSecPos = [];
          startPos.push(e.touches[0].pageX);
          startPos.push(e.touches[0].pageY);
          startSecPos.push(e.touches[1].pageX);
          startSecPos.push(e.touches[1].pageY);

          left = Number(style.getPropertyValue('transform').split(' ')[4].replace(',', ''));
          top = Number(style.getPropertyValue('transform').split(' ')[5].replace(')', ''));
        }
      }, false);

      element.addEventListener('touchend', function (e) {
        draggable = false;
        attention.classList.remove('active');
      }, false);

      element.addEventListener('touchmove', function (event) {
        if (event.touches.length > 1) {
          event.preventDefault();
          if (draggable === true) {
            let diffX = startPos[0] - event.touches[0].pageX,
              diffY = startPos[1] - event.touches[0].pageY;

            newX = left - diffX;
            newY = top - diffY;

            checkBound();
            element.style.transform = 'translate3d(' + newX + 'px, ' + newY + 'px, 0) scale(' + currentZoom + ')';
          }
        } else {
          attention.classList.add('active');
        }
      }, false);
    }
  }

  function zoomInByButtonDesktop() {
    if (currentZoom < maxZoom) {
      let left, top;
      currentZoom = currentZoom + zoomStep;

      let ratio = (element.offsetWidth * currentZoom) / (element.offsetWidth * (currentZoom - zoomStep));

      left = Number(style.getPropertyValue('transform').split(' ')[4].replace(',', ''));
      top = Number(style.getPropertyValue('transform').split(' ')[5].replace(')', ''));

      let x = Math.abs(left) + (boundWidth / 2);
      let y = Math.abs(top) + (boundHeight / 2);

      let newX = left - (x * ratio - x);
      let newY = top - (y * ratio - y);

      let boundXOffset = boundWidth - elWidth * currentZoom,
        boundYOffset = boundHeight - elHeight * currentZoom;

      if (newX > 0) {
        newX = 0;
      } else if (newX < boundXOffset) {
        newX = boundXOffset;
      }

      if (newY > 0) {
        newY = 0;
      } else if (newY < boundYOffset) {
        newY = boundYOffset;
      }

      element.style.transform = 'translate3d(' + newX + 'px, ' + newY + 'px, 0) scale(' + currentZoom + ')';
      element.style.transition = 'transform .2s linear';

      setTimeout(function () {
        element.style.transition = 'none';
      }, 250)
    }
  }

  function zoomInByButtonMobile(centerZoom) {
    if (currentZoom < maxZoom) {
      let left, top, centerZoom;
      currentZoom = currentZoom + zoomStep;

      if (centerZoom) {
        centerZoom = centerZoom;
      } else {
        centerZoom = boundWidth / 2;
      }

      let ratio = (element.offsetWidth * currentZoom) / (element.offsetWidth * (currentZoom - zoomStep));

      left = Number(style.getPropertyValue('transform').split(' ')[4].replace(',', ''));
      top = Number(style.getPropertyValue('transform').split(' ')[5].replace(')', ''));

      let x = Math.abs(left) + centerZoom;
      let y = Math.abs(top) + centerZoom;

      let newX = left - (x * ratio - x);
      let newY = top - (y * ratio - y);

      let boundXOffset = boundWidth - elWidth * currentZoom,
        boundYOffset = boundHeight - elHeight * currentZoom;

      if (newX > 0) {
        newX = 0;
      } else if (newX < boundXOffset) {
        newX = boundXOffset;
      }

      if (newY > 0) {
        newY = 0;
      } else if (newY < boundYOffset) {
        newY = boundYOffset;
      }

      element.style.transform = 'translate3d(' + newX + 'px, ' + newY + 'px, 0) scale(' + currentZoom + ')';
      element.style.transition = 'transform .2s linear';

      setTimeout(function () {
        element.style.transition = 'none';
      }, 250)
    }
  }

  function zoomOutByButtonDesktop(centerZoom) {
    if (currentZoom.toFixed(1) > defaultZoom) {
      let left, top, centerZoom;

      if (centerZoom) {
        centerZoom = centerZoom;
      } else {
        centerZoom = boundWidth / 2;
      }

      currentZoom = currentZoom - zoomStep;
      let ratio = (element.offsetWidth * currentZoom) / (element.offsetWidth * (currentZoom + zoomStep));

      left = Number(style.getPropertyValue('transform').split(' ')[4].replace(',', ''));
      top = Number(style.getPropertyValue('transform').split(' ')[5].replace(')', ''));

      let x = Math.abs(left) + centerZoom;
      let y = Math.abs(top) + centerZoom;

      let newX = left - (x * ratio - x);
      let newY = top - (y * ratio - y);

      let boundXOffset = boundWidth - elWidth * currentZoom,
        boundYOffset = boundHeight - elHeight * currentZoom;

      if (newX > 0) {
        newX = 0;
      } else if (newX < boundXOffset) {
        newX = boundXOffset;
      }

      if (newY > 0) {
        newY = 0;
      } else if (newY < boundYOffset) {
        newY = boundYOffset;
      }

      element.style.transform = 'translate3d(' + newX + 'px, ' + newY + 'px, 0) scale(' + currentZoom + ')';
      element.style.transition = 'transform .2s linear';

      setTimeout(function () {
        element.style.transition = 'none';
      }, 250)
    }
  }

  function zoomOutByButtonMobile() {
    if (currentZoom.toFixed(1) > mobileZoom) {
      let left, top, centerZoom;

      if (centerZoom) {
        centerZoom = centerZoom;
      } else {
        centerZoom = boundWidth / 2;
      }

      currentZoom = currentZoom - zoomStep;
      let ratio = (element.offsetWidth * currentZoom) / (element.offsetWidth * (currentZoom + zoomStep));

      left = Number(style.getPropertyValue('transform').split(' ')[4].replace(',', ''));
      top = Number(style.getPropertyValue('transform').split(' ')[5].replace(')', ''));

      let x = Math.abs(left) + centerZoom;
      let y = Math.abs(top) + centerZoom;

      let newX = left - (x * ratio - x);
      let newY = top - (y * ratio - y);

      let boundXOffset = boundWidth - elWidth * currentZoom,
        boundYOffset = boundHeight - elHeight * currentZoom;

      if (newX > 0) {
        newX = 0;
      } else if (newX < boundXOffset) {
        newX = boundXOffset;
      }

      if (newY > 0) {
        newY = 0;
      } else if (newY < boundYOffset) {
        newY = boundYOffset;
      }

      element.style.transform = 'translate3d(' + newX + 'px, ' + newY + 'px, 0) scale(' + currentZoom + ')';
      element.style.transition = 'transform .2s linear';

      setTimeout(function () {
        element.style.transition = 'none';
      }, 250)
    }
  }

  function checkBound() {
    let boundXOffset = boundWidth - elWidth * currentZoom,
      boundYOffset = boundHeight - elHeight * currentZoom;

    if (elWidth * currentZoom < boundWidth && elHeight * currentZoom < boundHeight) {
      if (newX < 0) newX = 0;
      if (newY < 0) newY = 0;
      if (newX > boundXOffset) newX = boundXOffset;
      if (newY > boundYOffset) newY = boundYOffset;
    } else if (elWidth * currentZoom > boundWidth && elHeight * currentZoom > boundHeight) {
      if (newX > 0) newX = 0;
      if (newY > 0) newY = 0;
      if (newX < boundXOffset) newX = boundXOffset;
      if (newY < boundYOffset) newY = boundYOffset;
    } else if (elWidth * currentZoom > boundWidth) {
      if (newX > 0) newX = 0;
      if (newX < boundXOffset) newX = boundXOffset;
      if (newY < 0) newY = 0;
      if (newY > boundYOffset) newY = boundYOffset;
    } else if (elHeight * currentZoom > boundHeight) {
      if (newY > 0) newY = 0;
      if (newY < boundYOffset) newY = boundYOffset;
      if (newX < 0) newX = 0;
      if (newX > boundXOffset) newX = boundXOffset;
    }
  }
}