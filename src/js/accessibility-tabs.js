import makeInteractive from './makeInteractive/makeInteractive';
export default function () {
  const mapTabs = $('.js-map-tabs'),
    modesListContainer = $('.js-map-modes-container'),
    mainMap = $('.js-main-map-container'),
    mainInfra = $('.js-main-infra-container'),
    $newsPanel = $('.js-news-panel'),
    activeClass = 'active';

  mapTabs.click(function () {
    const thisIndex = $(this).index();

    if (!$(this).hasClass(activeClass)) {
      mapTabs.removeClass(activeClass);
      $(this).addClass(activeClass);

      modesListContainer.children().hide();
      modesListContainer.children().eq(thisIndex).fadeIn();

      if ($(this).index() === 0) {
        mainInfra.hide()
        mainMap.show()
        $newsPanel.removeClass('hidden');
      } else {
        mainMap.hide();
        mainInfra.show();
        makeInteractive('.js-draggable-block', {
          zoomable: true,
          defaultZoom: 1,
          mobileZoom: .6,
          zoomByScroll: false,
          zoomStep: .2,
          draggable: true,
          bound: '.js-draggable-bound',
          mobileAttention: '.js-infra-attention',
          defaultPosition: [0, 0],
          mobileDefaultPosition: [20, 0]
        });
        $newsPanel.addClass('hidden');
      }
    }
  })
}