module.exports = function () {
  const infraListClick = $('.js-infra-list-click'),
    infraListItem = $('.js-infra-list-item'),
    infraDesBlock = $('.js-infra-des'),
    infraList = $('.js-infra-list'),
    $shieldsContainer = $('.js-infra-shields'),
    openedClass = 'opened';

  infraListClick.click(function () {
    listClick($(this).parent());
  });

  $shieldsContainer.children().click(function () {
    const thisType = $(this).data('type');
    listClick(infraListItem.filter('[data-type=' + thisType + ']'));
  });

  infraList.mCustomScrollbar();

  function listClick(el) {
    const typeId = el.data('type');

    if (el.hasClass(openedClass)) {
      infraListItem.removeClass(openedClass);
      infraDesBlock.slideUp();
      $shieldsContainer.children().show()
    } else {
      infraListItem.removeClass(openedClass);
      infraDesBlock.slideUp();

      $shieldsContainer.children().hide()
      $shieldsContainer.children().filter('div[data-type=' + typeId + ']').show();

      el.addClass(openedClass);
      el.children('.js-infra-des').slideDown();
    }
  }
}