import makeInteractive from './makeInteractive/makeInteractive';
export default function () {
  const $newsPanel = $('.js-news-panel'),
    $newsOpenClose = $('.js-news-closeopen-button'),
    $newsCategoriesTabs = $('.js-metro-news-tab'),
    $newsTabs = $('.js-accessibility-news-list-tabs'),
    $newsTab = $('.js-accessibility-news-list-tab'),
    $newsLists = $('.js-accessibility-news-lists'),
    $newsList = $('.js-accessibility-news-list'),
    $newsContainer = $('.js-metro-news-container'),
    mainMap = $('.js-main-map-container'),
    $mapNewsContainer = $('.js-map-news'),
    activeClass = 'active';

  let index = 0;

  $newsTabs.slick();
  slickNewsTabs();

  $newsCategoriesTabs.click(function () {
    const thisIndex = $(this).index();

    if (thisIndex === 0) {
      activateFirstMap();
    } else {
      activateSecMap();
    }

    $newsCategoriesTabs.removeClass(activeClass);
    $(this).addClass(activeClass);
    $newsContainer.children().hide().removeClass(activeClass);
    $newsContainer.children().eq(thisIndex).show().addClass(activeClass);

    slickNewsTabs();
  });

  $newsTabs.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    $newsList.hide();
    $newsContainer.children('.active').find('.js-accessibility-news-list').eq(nextSlide).show();
  });

  $newsPanel.on('click', function (e) {
    if (e.target.classList.value.indexOf('js-news-closeopen-button') > -1) {
      $newsPanel.toggleClass(activeClass);
      if ($newsPanel.hasClass(activeClass)) {
        mainMap.hide();
        $mapNewsContainer.show();
        activateFirstMap();
      } else {
        $mapNewsContainer.hide();
        mainMap.show();
      }
    } else {
      if (!$newsPanel.hasClass(activeClass)) {
        $newsPanel.addClass(activeClass);
        mainMap.hide();
        $mapNewsContainer.show();
        activateFirstMap();
      }
    }
  });

  $newsList.mCustomScrollbar();

  $newsTab.click(function () {
    const thisIndex = $(this).index();
    $newsTabs.slick('slickGoTo', thisIndex, false);
  });

  function activateFirstMap() {
    $('.js-map-news-draggable-second').hide();
    $('.js-map-news-draggable-first').show();
    makeInteractive('.js-map-news-draggable-first', {
      draggable: true,
      bound: '.js-map-news',
      defaultPosition: [0, 0],
      mobileDefaultPosition: [20, 0]
    });
  }

  function activateSecMap() {
    $('.js-map-news-draggable-first').hide();
    $('.js-map-news-draggable-second').show();
    makeInteractive('.js-map-news-draggable-second', {
      draggable: true,
      bound: '.js-map-news',
      defaultPosition: [0, 0],
      mobileDefaultPosition: [20, 0]
    });
  }

  function slickNewsTabs() {
    let options;
    if (window.innerWidth < 720) {
      options = {
        infinite: false,
        slidesToShow: 1
      }
    } else {
      options = {
        infinite: false,
        variableWidth: true
      }
    }
    $newsTabs.slick('unslick');
    $newsContainer.children('.active').find('.js-accessibility-news-list-tabs').slick(options);
    $newsContainer.children('.active').find('.js-accessibility-news-list').eq(0).show();
  }
}