module.exports = function () {
  setTimeout(function () {

    $('.accessibility_page__container').addClass('active');

  }, 1000);

  var myMap, myGeoObjects, additionalPointsTemplate, lowestPathIndex, makePathActive, checkLowestPath,
    addAdditionalPoints, returnAdditionalPoints, deleteAditionalPoints, makeShieldActive, makeShieldUnactive, whiteMap,
    normalMap, parseRoutes, removeRoutes, $mapBack, $mapLayer, routesCollectionFirst, routesCollectionSec,
    routesCollectionThrd, popUpCollection, home, addPointsFirstCollect, addPointsSecCollect, routesGlow, homePopUp;


// Высота контейнера со списком точек для скроллбара

  // var listHeight = $('.accessibility-list').height() - 50;
  // $('.accessibility-list__container').height(listHeight);
  //
  // $(window).resize(function () {
  //
  //   listHeight = $('.accessibility-list').height() - 50;
  //   $('.accessibility-list__container').height(listHeight);
  // });

// Скролбар на панели со списком точек

  $('.accessibility-list__container').mCustomScrollbar();

// Открытие -  закрытие карты

  var i = 0;

  $('.accessibility__panel__button').on('click', function () {

    if (i == 0 || i % 2 == 0) {

      $('.accessibility__panel').addClass('closed');
      $('.accessibility__panel__button.mobile').children('div').eq(0).removeClass('active');
      $('.accessibility__panel__button.mobile').children('div').eq(1).addClass('active');

      i++;
    } else {

      $('.accessibility__panel').removeClass('closed');
      $('.accessibility__panel__button.mobile').children('div').eq(1).removeClass('active');
      $('.accessibility__panel__button.mobile').children('div').eq(0).addClass('active');

      i++;
    }
  });

// Добавление всех точек из списка

  var loadMainShields = function () {

    $('.points__item_container').each(function () {

      if ($(this).hasClass('active') !== true) {

        var shieldCoords = [];
        var shieldName = $(this).data('name'),
          shieldTime = $(this).data('time'),
          shieldId = $(this).data('routeid'),
          pointChilds = $(this).data('childs'),
          shieldType = $(this).data('type');

        if ($(this).data('latitude') !== undefined && $(this).data('longitude') !== undefined) {
          shieldCoords.push($(this).data('latitude'));
          shieldCoords.push($(this).data('longitude'));
        }

        if (shieldType == 'metro') {

          var myPlacemark = new ymaps.Placemark(shieldCoords, {}, {
            iconLayout: 'default#image',
            iconImageHref: '/assets/img/map_icons/metro_not_active.png',
            iconImageSize: [56, 70],
            iconImageOffset: [-20, -60],
            opacity: 0.2,
            name: shieldName,
            time: shieldTime,
            type: 'social',
            pointId: shieldId,
            childs: pointChilds,
            objectType: 'point',
            active: 'yes'
          });
        } else if (shieldType == 'airport') {

          var myPlacemark = new ymaps.Placemark(shieldCoords, {}, {
            iconLayout: 'default#image',
            iconImageHref: '/assets/img/map_icons/airport_not_active.png',
            iconImageSize: [56, 70],
            iconImageOffset: [-20, -60],
            opacity: 1,
            name: shieldName,
            time: shieldTime,
            type: 'social',
            pointId: shieldId,
            childs: pointChilds,
            objectType: 'point',
            active: 'yes'
          });
        } else if (shieldType == 'highway') {

          var myPlacemark = new ymaps.Placemark(shieldCoords, {}, {
            iconLayout: 'default#image',
            iconImageHref: '/assets/img/map_icons/highway_not_active.png',
            iconImageSize: [56, 70],
            iconImageOffset: [-20, -60],
            opacity: 1,
            name: shieldName,
            time: shieldTime,
            type: 'social',
            pointId: shieldId,
            childs: pointChilds,
            objectType: 'point',
            active: 'yes'
          });
        } else if (shieldType == 'mcad') {

          var myPlacemark = new ymaps.Placemark(shieldCoords, {}, {
            iconLayout: 'default#image',
            iconImageHref: '/assets/img/map_icons/mcad_not_active.png',
            iconImageSize: [56, 70],
            iconImageOffset: [-20, -60],
            opacity: 1,
            name: shieldName,
            time: shieldTime,
            type: 'social',
            pointId: shieldId,
            childs: pointChilds,
            objectType: 'point',
            active: 'yes'
          });
        }

        myMap.geoObjects.add(myPlacemark);

      }

    });
  };

// Тогл на списке поинтов

  var toggleList = function (toogleE) {

    var listParent = toogleE.parents('.points__item_container');

    if (listParent.hasClass('active') == true) {

      toogleE.find('.points__item_opener').removeClass('opened');

      listParent.removeClass('active');

      listParent.children('.points__additional_container').slideToggle();

      if (toogleE.parents('.toggle').attr('data-colId') == 1) {

        addPointsFirstCollect.removeAll();

        returnPoint(toogleE);
      } else {

        addPointsSecCollect.removeAll();
        returnPoint(toogleE);
      }
    } else {

      $('.points__item_container').each(function () {

        if ($(this).hasClass('active') == true) {

          $(this).removeClass('active');
          $(this).find('.points__item_opener').removeClass('opened');
          $(this).children('.points__additional_container').slideToggle();
          deleteAditionalPoints($(this));

          if ($(this).attr('data-colId') == 1) {

            addPointsFirstCollect.removeAll();
            returnPoint($(this).children('.points__additional_container'));
          } else {

            addPointsSecCollect.removeAll();
            returnPoint($(this).children('.points__additional_container'));
          }
        }
      });

      toogleE.find('.points__item_opener').addClass('opened');

      listParent.addClass('active');

      listParent.children('.points__additional_container').slideToggle();

      addAdditionalPoints(listParent.find('.points__info_container'));
    }
  };

  var returnPoint = function (e) {

    var pointCoords = [];
    pointCoords.push(e.parents('.toggle').data('latitude'));
    pointCoords.push(e.parents('.toggle').data('longitude'));
    var shieldName = e.parents('.toggle').data('name');
    var shieldTime = e.parents('.toggle').data('time');
    var shieldId = e.parents('.toggle').data('routeid');
    var pointChilds = e.parents('.toggle').data('childs');

    var togglePoint = new ymaps.Placemark(pointCoords, {}, {
      iconLayout: 'default#image',
      iconImageHref: '/assets/img/map_icons/metro_not_active.png',
      iconImageSize: [56, 70],
      iconImageOffset: [-20, -60],
      name: shieldName,
      time: shieldTime,
      type: 'social',
      objectType: 'point',
      pointId: shieldId,
      childs: pointChilds,
      active: 'yes'
    });

    myMap.geoObjects.add(togglePoint);
  };

//Добавляем глоу на карту

  var addGlow = function (e) {

    var startPoint = [],
      endPoint = [],
      routeColor = e.attr('data-color'),
      routeType = e.data('pointtype'),
      rowNumber = e.attr('data-rowNumber'),
      viaPoint = [],
      viaPointSec = [],
      viaPointThr = [];

    startPoint.push(e.attr('data-startLatitude'));
    startPoint.push(e.attr('data-startLongitude'));

    endPoint.push(e.attr('data-endLatitude'));
    endPoint.push(e.attr('data-endLongitude'));

    if (e.attr('data-viaPointLat') !== undefined) {
      viaPoint.push(e.attr('data-viaPointLat'));
      viaPoint.push(e.attr('data-viaPointLong'));
    }

    if (e.attr('data-viaPointSecLat') !== undefined) {
      viaPointSec.push(e.attr('data-viaPointSecLat'));
      viaPointSec.push(e.attr('data-viaPointSecLong'));
    }

    if (e.attr('data-viaPointThrLat') !== undefined) {
      viaPointThr.push(e.attr('data-viaPointThrLat'));
      viaPointThr.push(e.attr('data-viaPointThrLong'));
    }

    if (routeType == 'metro') {

      var myPolyline = new ymaps.Polyline([startPoint, endPoint], {}, {
        strokeWidth: 8,
        strokeColor: routeColor,
        draggable: false,
        opacity: 0.2
      });

      var myPolyline = new ymaps.Polyline([startPoint, endPoint], {}, {
        strokeWidth: 14,
        strokeColor: routeColor,
        draggable: false,
        opacity: 0.1
      });

      routesGlow.add(myPolyline);
    } else if (routeType == 'line') {

      var routePoints = [];

      if (viaPointSec.length > 0 && viaPointThr.length > 0) {
        routePoints.push(startPoint, viaPoint, viaPointSec, viaPointThr, endPoint);
      } else if (viaPointSec.length > 0) {
        routePoints.push(startPoint, viaPoint, viaPointSec, endPoint);
      } else {
        routePoints.push(startPoint, viaPoint, endPoint);
      }

      var myPolylineF = new ymaps.Polyline(routePoints, {}, {
        strokeWidth: 8,
        strokeColor: routeColor,
        draggable: false,
        opacity: 0.2
      });

      var myPolylineS = new ymaps.Polyline(routePoints, {}, {
        strokeWidth: 14,
        strokeColor: routeColor,
        draggable: false,
        opacity: 0.1
      });

      routesGlow.add(myPolylineF, myPolylineS);
    } else {

      ymaps.route([startPoint, endPoint], {type: 'route'}).then(function (route) {
        route.getPaths().options.set({
          strokeColor: routeColor,
          strokeWidth: [10],
          type: 'route',
          routeId: rowNumber,
          opacity: 0.2
        });

        routesGlow.add(route.getPaths());
      }, function (error) {
        console.log('Возникла ошибка: ' + error.message);
      });

      ymaps.route([startPoint, endPoint], {type: 'route'}).then(function (route) {
        route.getPaths().options.set({
          strokeColor: routeColor,
          strokeWidth: [15],
          type: 'route',
          routeId: rowNumber,
          opacity: 0.1
        });

        routesGlow.add(route.getPaths());
      }, function (error) {
        console.log('Возникла ошибка: ' + error.message);
      });
    }

    ymaps.route([startPoint, startPoint], {type: 'route'}).then(function (route) {
      route.getPaths().options.set({
        strokeColor: ['#ffffff', routeColor],
        strokeWidth: [18, 11]
      });

      setTimeout(function () {

        routesGlow.add(route.getPaths());
      }, 100);
    }, function (error) {
      console.log('Возникла ошибка: ' + error.message);
    });

    ymaps.route([endPoint, endPoint], {type: 'route'}).then(function (route) {
      route.getPaths().options.set({
        strokeColor: ['#ffffff', routeColor],
        strokeWidth: [18, 11]
      });

      setTimeout(function () {

        routesGlow.add(route.getPaths());
      }, 100);
    }, function (error) {
      console.log('Возникла ошибка: ' + error.message);
    });

    myGeoObjects.add(routesGlow);
  };

  ymaps.ready(function () {

    init();
    loadMainShields();

    $('ymaps').each(function () {

      if ($(this).attr('class') !== undefined) {

        if ($(this).attr('class').indexOf('-ground-pane') > 0) {

          $mapLayer = $(this).attr('class');

          console.log($mapLayer);
        }

      }

    });

    $mapBack = $('#transportMap ymaps');
  });

  whiteMap = function () {

    $mapBack.css('background', 'none');
    $("." + $mapLayer + "").animate({
      opacity: 0.4
    }, 400);
  };

  normalMap = function () {

    $("." + $mapLayer + "").animate({
      opacity: 1
    }, 400);
  };

  function init() {

    if ($(window).width() < 500) {
      myMap = new ymaps.Map("transportMap", {
        center: [55.590521, 37.453550],
        zoom: 11,
        controls: ['zoomControl']
      });
    } else {
      myMap = new ymaps.Map("transportMap", {
        center: [55.625857, 37.661209],
        zoom: 11,
        controls: ['zoomControl']
      });
    }
    myMap.behaviors.disable('scrollZoom');

    myGeoObjects = myMap.geoObjects;

    // Коллекции объектов на карте

    routesCollectionFirst = new ymaps.GeoObjectCollection();
    routesCollectionSec = new ymaps.GeoObjectCollection();
    routesCollectionThrd = new ymaps.GeoObjectCollection();
    popUpCollection = new ymaps.GeoObjectCollection();
    home = new ymaps.GeoObjectCollection();
    addPointsFirstCollect = new ymaps.GeoObjectCollection();
    addPointsSecCollect = new ymaps.GeoObjectCollection();
    routesGlow = new ymaps.GeoObjectCollection();

    // Шаблон попапа для дополнительных точек

    additionalPointsTemplate = ymaps.templateLayoutFactory.createClass(['<ymaps class="multilined-icon ">', '<ymaps class="label small">{{ properties.iconContent|raw }}<ymaps class="label_time">{{ properties.iconTime|raw }} <span> мин.</span></ymaps></ymaps>', '</ymaps>'].join(''), {
      getShape: function () {
        var width = 0;
        var height = 0;
        var position = 0;

        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([[position.left, position.top], [position.left - width, position.top - height]]));
      }
    });

    // Шаблон попапа

    var MultipleLinedIconLayout = ymaps.templateLayoutFactory.createClass(['<ymaps class="multilined-icon">', '<ymaps class="label">{{ properties.iconContent|raw }}<ymaps class="label_time">{{ properties.iconTime|raw }} <span> мин.</span></ymaps></ymaps>', '</ymaps>'].join(''), {
      getShape: function () {
        var width = 0;
        var height = 0;
        var position = 0;

        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([[position.left, position.top], [position.left + width, position.top + height]]));
      }
    });

    // Шаблон попапа для комплекса

    homePopUp = ymaps.templateLayoutFactory.createClass(['<ymaps class="multilined-icon ">', '<ymaps class="label home">{{ properties.iconContent|raw }}</ymaps>', '</ymaps>'].join(''), {
      getShape: function () {
        var width = 0;
        var height = 0;
        var position = 0;

        return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([[position.left, position.top], [position.left - width, position.top - height]]));
      }
    });

    // Добавление комплекса

    var addHome = function (e) {

      homePoint = [];
      homePoint.push(e.attr('data-Latitude'));
      homePoint.push(e.attr('data-Longitude'));

      var homePoint = new ymaps.Placemark(homePoint, {}, {
        iconLayout: 'default#image',
        iconImageHref: '/assets/img/map_icons/home_icon_active.png',
        iconImageSize: [74, 90],
        iconImageOffset: [-37, -90],
        type: 'home',
        name: 'Испанские кварталы',
        time: '',
        active: 'yes'
      });

      home.add(homePoint);
      myMap.geoObjects.add(home);
    };

    addHome($('.accessibility__title'));

    // Вывод подписей при наведении

    var pointDescription;

    myGeoObjects.events.add('mouseenter', function (e) {

      if (e.get('target').options.get('active') == 'yes') {

        var pointTitle = e.get('target').options.get('name');
        var pointType = e.get('target').options.get('type');
        var pointTime = e.get('target').options.get('time');

        if (pointTitle !== undefined || pointTime !== undefined) {

          var template;

          if (pointType == 'social') {

            template = MultipleLinedIconLayout;
          } else if (pointType == 'home') {

            template = homePopUp;
          } else {

            template = additionalPointsTemplate;
          }

          var pointCoords = e.get('target').geometry.getCoordinates();

          pointDescription = new ymaps.Placemark(pointCoords, {
            iconContent: pointTitle,
            iconTime: pointTime
          }, {
            iconLayout: template
          });

          popUpCollection.add(pointDescription);

          myGeoObjects.add(popUpCollection);

          $('#transportMap').find('label').each(function () {

            $(this).parents().css('pointerEvents', 'none');
            $(this).parents().eq(1).css('pointerEvents', 'none');
          });
        }

      }

    }).add('mouseleave', function (e) {

      if (e.get('target').options.get('active') == 'yes') {

        e.get('target').options.unset('preset');
        popUpCollection.removeAll();

      }

    });

    var addDescription = function (elem) {

      var pointTitle = elem.options.get('name');
      var pointTime = elem.options.get('time');

      template = MultipleLinedIconLayout;

      var pointCoords = elem.geometry.getCoordinates();

      var pointDescription = new ymaps.Placemark(pointCoords, {
        iconContent: pointTitle,
        iconTime: pointTime
      }, {
        iconLayout: template
      });

      popUpCollection.add(pointDescription);

      myGeoObjects.add(popUpCollection);

      $('#transportMap').find('label').each(function () {

        $(this).parents().css('pointerEvents', 'none');
        $(this).parents().eq(1).css('pointerEvents', 'none');
      });

    }

    // Добавление дополнительных точек

    addAdditionalPoints = function (em) {

      var additionalPoints = em.siblings().find('.points__additional_item');

      additionalPoints.each(function () {

        var pointCoords = [];
        pointCoords.push($(this).data('latitude'));
        pointCoords.push($(this).data('longitude'));

        var pointName = $(this).data('name');
        var pointTime = $(this).data('time');
        var pointId = $(this).data('routeid');
        var pointColor = $(this).data('color');

        var additionalPoint = new ymaps.Placemark(pointCoords, {}, {
          iconLayout: 'default#image',
          iconImageHref: '/assets/img/' + pointColor + '_add_point_active.png',
          iconImageSize: [18, 18],
          iconImageOffset: [-9, -9],
          name: pointName,
          time: pointTime,
          type: 'social_mini',
          pointId: pointId,
          objectType: 'point',
          active: 'yes'
        });

        if (em.parents('.toggle').attr('data-colId') == 1) {

          addPointsFirstCollect.add(additionalPoint);
        } else {

          addPointsSecCollect.add(additionalPoint);
        }
      });

      myGeoObjects.add(addPointsFirstCollect);
      myGeoObjects.add(addPointsSecCollect);
    };

    // Удаление дополнительных точек

    deleteAditionalPoints = function (e) {

      if (e.parents('.toggle').attr('data-colId') == 1) {

        addPointsFirstCollect.removeAll();
      } else {

        addPointsSecCollect.removeAll();
      }
    };

    returnAdditionalPoints = function () {

      addPointsFirstCollect.removeAll();
      addPointsSecCollect.removeAll();

      $('.points__item_container').each(function () {

        if ($(this).hasClass('active') == true) {

          addAdditionalPoints($(this).find('.points__info_container'));

        }

      });

    };

    // Активный шилд по клику на карте

    makeShieldActive = function (e) {

      popUpCollection.removeAll();

      myGeoObjects.each(function (pm) {

        if (pm.options.get('objectType') == 'point') {

          if (pm.options.get('pointId') == e) {

            addDescription(pm);

            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("not_active", "active") + '');
            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("_show", "_hide") + '');
            pm.options.set('active', 'ep');

          } else {

            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("not_active", "none") + '');
            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("_show", "_hide") + '');
            pm.options.set('active', 'none');

          }
        }
      });

      addPointsFirstCollect.each(function (pm) {

        if (pm.options.get('objectType') == 'point') {

          if (pm.options.get('pointId') == e) {

            addDescription(pm);

            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("_hide", "_show") + '');
            pm.options.set('active', 'ep');

          } else {

            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("_show", "_hide") + '');
            pm.options.set('active', 'none');

          }
        }
      });

      addPointsSecCollect.each(function (pm) {

        if (pm.options.get('objectType') == 'point') {

          if (pm.options.get('pointId') == e) {

            addDescription(pm);

            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("_hide", "_show") + '');
            pm.options.set('active', 'ep');

          } else {

            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("_show", "_hide") + '');
            pm.options.set('active', 'none');

          }
        }
      });
    };

    // Фукнция анэктив шилда на карте

    makeShieldUnactive = function (e) {

      popUpCollection.removeAll();

      myGeoObjects.each(function (pm) {

        if (pm.options.get('iconImageHref') !== undefined && pm.options.get('type') == 'social') {

          if (pm.options.get('iconImageHref').indexOf('not_active') + 1 == 0) {

            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("active", "not_active") + '');
            pm.options.set('iconImageHref', '' + pm.options.get('iconImageHref').replace("none", "not_active") + '');
            pm.options.set('active', 'yes');
          }
        }
      });
    };

    // Добавление маршрутов из активной карты

    parseRoutes = function (e) {

      var routeId = e.data('routeid');

      var $routeContainer = $(".routes__container[data-routeId='" + routeId + "']");

      $routeContainer.find('.routes__line').each(function () {

        var startPoint = [],
          endPoint = [],
          routeColor = $(this).data('color'),
          rowNumber = $(this).attr('data-rowNumber'),
          lineId = $(this).attr('data-lineId'),
          pointType = $(this).data('pointtype'),
          viaPoint = [],
          viaPointSec = [],
          viaPointThr = [];

        startPoint.push($(this).attr('data-startLatitude'));
        startPoint.push($(this).attr('data-startLongitude'));

        endPoint.push($(this).attr('data-endLatitude'));
        endPoint.push($(this).attr('data-endLongitude'));

        if ($(this).attr('data-viaPointLat') !== undefined) {
          viaPoint.push($(this).attr('data-viaPointLat'));
          viaPoint.push($(this).attr('data-viaPointLong'));
        }

        if ($(this).attr('data-viaPointSecLat') !== undefined) {
          viaPointSec.push($(this).attr('data-viaPointSecLat'));
          viaPointSec.push($(this).attr('data-viaPointSecLong'));
        }

        if ($(this).attr('data-viaPointThrLat') !== undefined) {
          viaPointThr.push($(this).attr('data-viaPointThrLat'));
          viaPointThr.push($(this).attr('data-viaPointThrLong'));
        }

        if (rowNumber == 1) {

          if (pointType == 'metro') {

            var myPolyline = new ymaps.Polyline([startPoint, endPoint], {}, {
              strokeWidth: 5,
              strokeColor: routeColor,
              draggable: false,
              opacity: 1,
              objectType: 'route',
              routeId: lineId
            });

            routesCollectionFirst.add(myPolyline);
          } else if (pointType == 'line') {

            var routePoints = [];

            if (viaPointSec.length > 0 && viaPointThr.length > 0) {
              routePoints.push(startPoint, viaPoint, viaPointSec, viaPointThr, endPoint);
            } else if (viaPointSec.length > 0) {
              routePoints.push(startPoint, viaPoint, viaPointSec, endPoint);
            } else {
              routePoints.push(startPoint, viaPoint, endPoint);
            }

            var myPolyline = new ymaps.Polyline(routePoints, {}, {
              strokeWidth: 5,
              strokeColor: routeColor,
              draggable: false,
              opacity: 1,
              objectType: 'route',
              routeId: lineId
            });

            routesCollectionFirst.add(myPolyline);
          } else {

            ymaps.route([startPoint, endPoint], {}).done(function (route) {
              route.getPaths().options.set({
                strokeColor: routeColor,
                strokeWidth: [5],
                type: 'route',
                routeId: lineId,
                opacity: 1,
                objectType: 'route'
              });

              routesCollectionFirst.add(route.getPaths());
            }, function (error) {
              console.log('Возникла ошибка: ' + error.message);
            });
          }

          ymaps.route([startPoint, startPoint], {}).then(function (route) {
            route.getPaths().options.set({
              strokeColor: ['#ffffff', routeColor],
              strokeWidth: [18, 11],
              type: 'route',
              routeId: lineId,
              opacity: 1,
              objectType: 'route'
            });

            setTimeout(function () {
              routesCollectionFirst.add(route.getPaths());
            }, 500);
          }, function (error) {
            console.log('Возникла ошибка: ' + error.message);
          });

          if ($(this).data('last') == 1) {
            ymaps.route([endPoint, endPoint], {type: 'route'}).then(function (route) {
              route.getPaths().options.set({
                strokeColor: ['#ffffff', routeColor],
                strokeWidth: [18, 11],
                type: 'route',
                routeId: lineId,
                opacity: 1,
                objectType: 'route'
              });

              setTimeout(function () {
                routesCollectionFirst.add(route.getPaths());
              }, 500);
            }, function (error) {
              console.log('Возникла ошибка: ' + error.message);
            });
          }
        } else if (rowNumber == 2) {

          if (pointType == 'metro') {

            var myPolyline = new ymaps.Polyline([startPoint, endPoint], {}, {
              strokeWidth: 5,
              strokeColor: routeColor,
              draggable: false,
              opacity: 1,
              objectType: 'route',
              routeId: lineId
            });

            routesCollectionSec.add(myPolyline);
          } else if (pointType == 'line') {

            var routePoints = [];

            if (viaPointSec !== ['undefined', 'undefined'] && viaPointThr !== ['undefined', 'undefined']) {
              routePoints.push(startPoint, viaPoint, viaPointSec, viaPointThr, endPoint);
            } else if (viaPointSec !== ['undefined', 'undefined']) {
              routePoints.push(startPoint, viaPoint, viaPointSec, endPoint);
            } else {
              routePoints.push(startPoint, viaPoint, endPoint);
            }

            var myPolyline = new ymaps.Polyline(routePoints, {}, {
              strokeWidth: 5,
              strokeColor: routeColor,
              draggable: false,
              opacity: 1,
              objectType: 'route',
              routeId: lineId
            });

            routesCollectionSec.add(myPolyline);
          } else {

            ymaps.route([startPoint, endPoint], {}).done(function (route) {
              route.getPaths().options.set({
                strokeColor: routeColor,
                strokeWidth: [5],
                type: 'route',
                routeId: lineId,
                opacity: 1,
                objectType: 'route'
              });

              routesCollectionSec.add(route.getPaths());
            }, function (error) {
              console.log('Возникла ошибка: ' + error.message);
            });
          }

          ymaps.route([startPoint, startPoint], {}).then(function (route) {
            route.getPaths().options.set({
              strokeColor: ['#ffffff', routeColor],
              strokeWidth: [18, 11],
              type: 'route',
              routeId: lineId,
              opacity: 1,
              objectType: 'route'
            });

            setTimeout(function () {
              routesCollectionSec.add(route.getPaths());
            }, 500);
          }, function (error) {
            console.log('Возникла ошибка: ' + error.message);
          });

          if ($(this).data('last') == 1) {
            ymaps.route([endPoint, endPoint], {type: 'route'}).then(function (route) {
              route.getPaths().options.set({
                strokeColor: ['#ffffff', routeColor],
                strokeWidth: [18, 11],
                type: 'route',
                routeId: lineId,
                opacity: 1,
                objectType: 'route'
              });

              setTimeout(function () {
                routesCollectionSec.add(route.getPaths());
              }, 500);
            }, function (error) {
              console.log('Возникла ошибка: ' + error.message);
            });
          }
        } else if (rowNumber == 3) {

          if (pointType == 'metro') {

            var myPolyline = new ymaps.Polyline([startPoint, endPoint], {}, {
              strokeWidth: 5,
              strokeColor: routeColor,
              draggable: false,
              opacity: 1,
              objectType: 'route',
              routeId: lineId
            });

            routesCollectionThrd.add(myPolyline);
          } else if (pointType == 'line') {

            var routePoints = [];

            if (viaPointSec !== ['undefined', 'undefined'] && viaPointThr !== ['undefined', 'undefined']) {
              routePoints.push(startPoint, viaPoint, viaPointSec, viaPointThr, endPoint);
            } else if (viaPointSec !== ['undefined', 'undefined']) {
              routePoints.push(startPoint, viaPoint, viaPointSec, endPoint);
            } else {
              routePoints.push(startPoint, viaPoint, endPoint);
            }

            var myPolyline = new ymaps.Polyline(routePoints, {}, {
              strokeWidth: 5,
              strokeColor: routeColor,
              draggable: false,
              opacity: 1,
              objectType: 'route',
              routeId: lineId
            });

            routesCollectionThrd.add(myPolyline);
          } else {

            ymaps.route([startPoint, endPoint], {}).done(function (route) {
              route.getPaths().options.set({
                strokeColor: routeColor,
                strokeWidth: [5],
                type: 'route',
                routeId: lineId,
                opacity: 1,
                objectType: 'route'
              });

              routesCollectionThrd.add(route.getPaths());
            }, function (error) {
              console.log('Возникла ошибка: ' + error.message);
            });
          }

          ymaps.route([startPoint, startPoint], {}).then(function (route) {
            route.getPaths().options.set({
              strokeColor: ['#ffffff', routeColor],
              strokeWidth: [18, 11],
              type: 'route',
              routeId: lineId,
              opacity: 1,
              objectType: 'route'
            });

            setTimeout(function () {
              routesCollectionThrd.add(route.getPaths());
            }, 500);
          }, function (error) {
            console.log('Возникла ошибка: ' + error.message);
          });

          if ($(this).data('last') == 1) {
            ymaps.route([endPoint, endPoint], {type: 'route'}).then(function (route) {
              route.getPaths().options.set({
                strokeColor: ['#ffffff', routeColor],
                strokeWidth: [18, 11],
                type: 'route',
                routeId: lineId,
                opacity: 1,
                objectType: 'route'
              });

              setTimeout(function () {
                routesCollectionThrd.add(route.getPaths());
              }, 500);
            }, function (error) {
              console.log('Возникла ошибка: ' + error.message);
            });
          }
        }

        myMap.geoObjects.add(routesCollectionFirst);

        setTimeout(function () {
          myMap.geoObjects.add(routesCollectionSec);
          myMap.geoObjects.add(routesCollectionThrd);
        }, 1000);
      });
    };

    // Удаление маршрутов при закрытии карточки

    removeRoutes = function () {

      routesCollectionFirst.removeAll();
      routesCollectionSec.removeAll();
      routesCollectionThrd.removeAll();
    };

    // Клик на панели на путях

    var q = 0;

    var $panel = $('.accessibility__panel');
    var $header = $('.route-header__container');
    var $navContainer = $('.route-navigation__container');
    var $routeDes = $('#routeDes');

    var $headerTitle = $('#routeTitle');
    var $headerIcon = $('#routeIcon');
    var $navTime = $('#routeSlider');
    var $routePath = $('#routePath');

    var mobileRouteActive = function (p) {

      var title = $(document).find('.routes__container.active').find('.routes__title').text();
      var routeType = $(document).find('.routes__container.active').find('.routes__title').attr('class').replace('routes__title', '');

      $headerTitle.text(title);
      $headerIcon.addClass(routeType);

      $(document).find('.routes__container.active').find('.routes__row').each(function () {

        var $timeContainer = $(this).children('.routes__time');
        var time = $(this).children('.routes__time').text();
        var routePath = $(this).html();

        if ($timeContainer.length !== 0) {

          $navTime.append("<div class='route-navigation__time'>" + time + "</div>");
          $routePath.append("<div class='route-navigation__path'>" + routePath + "<div>");

        } else {

          $routePath.find('.route-navigation__path').append(routePath);

        }

        // Вывод попапа с инфой об общественном транспорте

        if ($(this).index() == p) {

          if ($(this).find('.route__desc-text').length > 0) {

            var desTitle = $(this).find('.route__desc-text').children('div').eq(0).text();
            var desText = $(this).find('.route__desc-text').children('div').eq(1).text();

            $('.route-description__title').text(desTitle);
            $('.route-description__des').text(desText);

            $routeDes.addClass('active');

          }

        }

      });

      $routePath.find('.routes__time').remove();
      $routePath.find('.route-navigation__path').eq(p).addClass('active');

      $navTime.slick({
        sliderToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        infinite: false,
        dots: true
      });
      $navTime.slick('slickGoTo', p, true);

      $navTime.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

        $routePath.find('.route-navigation__path').removeClass('active');
        $routePath.find('.route-navigation__path').eq(nextSlide).addClass('active');

        $routeDes.removeClass('active');

        var $textContainer = $routePath.find('.route-navigation__path').eq(nextSlide).find('.route__desc-text');

        if ($textContainer.length > 0) {

          var desTitle = $textContainer.children('div').eq(0).text();
          var desText = $textContainer.children('div').eq(1).text();

          $('.route-description__title').text(desTitle);
          $('.route-description__des').text(desText);

          setTimeout(function () {
            $routeDes.addClass('active');
          }, 300);

        }

        if ((nextSlide + 1) == 1) {

          routesCollectionFirst.each(function (e) {

            e.options.set({opacity: 1});
          });

          routesCollectionSec.each(function (e) {

            e.options.set({opacity: 0});
          });

        } else {

          routesCollectionFirst.each(function (e) {

            e.options.set({opacity: 0});
          });

          routesCollectionSec.each(function (e) {

            e.options.set({opacity: 1});
          });
        }

      });

      $panel.addClass('route-active');
      $header.addClass('active');
      $navContainer.addClass('active');

      if (p + 1 == 1) {

        routesCollectionFirst.each(function (e) {

          e.options.set({opacity: 1});
        });

        routesCollectionSec.each(function (e) {

          e.options.set({opacity: 0});
        });

      } else {

        routesCollectionFirst.each(function (e) {

          e.options.set({opacity: 0});
        });

        routesCollectionSec.each(function (e) {

          e.options.set({opacity: 1});
        });
      }

    };

    // Активируем путь (скрываем иной на карте а другой забеляем)

    makePathActive = function (a) {

      if ($(window).width() < 500) {


      } else {

        var lineNumber = a.attr('data-rowNumber');

        $('.routes__row').removeClass('active');
        a.addClass('active');
        routesCollectionFirst.each(function (e) {

          e.options.set({opacity: 1});

        });

        routesCollectionSec.each(function (e) {

          e.options.set({opacity: 1});
        });

        $('.route__container').addClass('disable');
        $(".routes__line[data-rowNumber='" + lineNumber + "']").parents('.route__container').removeClass('disable').addClass('active');
        $(".routes__line[data-rowNumber='3']").parents('.route__container').removeClass('disable').addClass('active');

        setTimeout(function () {

          if (lineNumber == 1) {

            routesCollectionFirst.each(function (e) {

              e.options.set({opacity: 1});
            });

            routesCollectionSec.each(function (e) {

              e.options.set({opacity: 0});
            });

          } else {

            routesCollectionFirst.each(function (e) {

              e.options.set({opacity: 0});
            });

            routesCollectionSec.each(function (e) {

              e.options.set({opacity: 1});
            });
          }

        }, 800);

      }


    };

    // Клик по строке с путями

    $('.routes__row').click(function () {

      $('.accessibility__panel__button').addClass('hidden')

      var rowIndex = $(this).index();
      var lineIndex = $(this).attr('data-rowNumber');


      if (window.matchMedia('(max-width: 768px)').matches) {
        mobileRouteActive(rowIndex);
      } else {

        if ($(this).hasClass('active') == true) {

          $('.routes__row').removeClass('active');
          $('.route__container').removeClass('disable').removeClass('active');
        } else {
          makePathActive($(this));
        }

      }
    });

    // Закрытие попапа с инфой об общественном транспорте

    $('.route-description__exit').click(function () {
      $('#routeDes').removeClass('active');
      $('.route-description__title').text('');
      $('.route-description__des').text('');
    })

    // Закрытие мобильной панели путей

    var closeMobileRoutes = function () {

      $header.removeClass('active');
      $navContainer.removeClass('active');
      $('.description__container').removeClass('active');

      if ($navTime.hasClass('slick-slider') == true) {

        $navTime.slick('unslick');

      }

      $navTime.children().remove();
      $routePath.children().remove();

      $('.route-description__title').text('');
      $('.route-description__des').text('');

      routesCollectionFirst.each(function (e) {

        e.options.set({opacity: 1});
      });
      routesCollectionSec.each(function (e) {

        e.options.set({opacity: 1});
      });
    };

    $('.route-header__container').on('click', function () {

      $('.accessibility__panel__button').removeClass('hidden')

      $panel.removeClass('route-active');
      $('#routeDes').removeClass('active');

      closeMobileRoutes();
    });

    // Расскрытие списка детей и добавление точек

    $('.toggle').children('.points__info_container').on('click', function () {

      var $pointParent = $(this).parents('.toggle');
      var pointName = $pointParent.data('name');

      if ($pointParent.hasClass('active') == true) {

        // deleteAditionalPoints($(this));

        // returnPoint($(this));

      } else {

        myGeoObjects.each(function (e) {

          if (e.options.get('name') == pointName) {

            myGeoObjects.remove(e);
          }
        });
        // addAdditionalPoints($(this));
      }

      toggleList($(this));
    });

    // Карточки маршрутов

    var $routesCardsList = $('.routes__list'),
      $routesCards = $('.routes__container');

    var actualRouteCard = function (routeId) {

      $routesCardsList.addClass('active');

      $routesCards.removeClass('active');
      $(".routes__container[data-routeId='" + routeId + "']").addClass('active');

      var centerCoords = [];
      if ($(window).width() < 500) {

        centerCoords.push(55.590521);
        centerCoords.push(37.453550);
      } else {

        centerCoords.push($(".routes__container[data-routeId='" + routeId + "']").attr('data-centerLat'));
        centerCoords.push($(".routes__container[data-routeId='" + routeId + "']").attr('data-centerLong'));
      }

      myMap.setCenter(centerCoords, $(".routes__container[data-routeId='" + routeId + "']").attr('data-zoom'));
    };

    // переход на карточку

    // Делаем путь активным

    checkLowestPath = function () {

      var $activePathContainer = $(document).find('.routes__container.active');
      var activePathTimes = [];

      $activePathContainer.find('.routes__time').each(function () {

        activePathTimes.push(parseInt($(this).text().replace('мин', '')));
      });

      var lowest = 0;

      function indexOfSmallest(a) {

        for (var i = 1; i < a.length; i++) {
          if (a[i] < a[lowest]) lowest = i;
          lowestPathIndex = lowest + 1;
        }

      };

      if (activePathTimes.length > 1) {

        indexOfSmallest(activePathTimes);

        makePathActive($(".routes__row[data-rowNumber='" + lowestPathIndex + "']"));

        if ($(window).width() < 500) {

          mobileRouteActive(lowestPathIndex - 1);

        }

      } else {

        makePathActive($(".routes__row[data-rowNumber='1']"));

        if ($(window).width() < 500) {

          mobileRouteActive(0);

        }

      }

    };

    $('.point').click(function () {

      $('.accessibility__panel__button').addClass('hidden');

      whiteMap();
      makeShieldActive($(this).data('routeid'));
      parseRoutes($(this));
      actualRouteCard($(this).data('routeid'));

      if ($(window).width() < 500) {

        if ($('.routes__container.active').length > 0) {

          $('.accessibility__header').addClass('unactive');
          $('.accessibility-list').addClass('active');

        }

      }

      checkLowestPath();
    });


    myGeoObjects.events.add('click', function (e) {

      if ($(window).width() < 500) {
        $('.accessibility__header').addClass('unactive');
        $('.accessibility-list').addClass('active');
      }

      var $list = $('.routes__list');

      var target = e.get('target');

      if (target.options.get('type') == 'social' || target.options.get('type') == 'social_mini') {

        if (e.get('target').options.get('childs') == true) {

          var pointName = target.options.get('name');

          var listItem = $(".toggle[data-name='" + pointName + "']");

          myGeoObjects.each(function (e) {

            if (e.options.get('name') == pointName) {

              myGeoObjects.remove(e);
            }
          });

          // addAdditionalPoints(listItem.children('.points__info_container'));

          popUpCollection.removeAll();

          toggleList(listItem.children('.points__additional_container'));

        } else {

          var pointId = target.options.get('pointId');

          if ($list.hasClass('active') !== true) {

            if ($(window).width() < 500) {
              closeMobileRoutes();
            }

            whiteMap();
            makeShieldActive(pointId);
            parseRoutes($(".point[data-routeId='" + pointId + "']"));
            actualRouteCard(pointId);

            checkLowestPath();

          } else {

            if ($(window).width() < 500) {

              closeMobileRoutes();

            }

            $list.removeClass('active');
            removeRoutes();
            routesGlow.removeAll();
            makeShieldUnactive();
            makeShieldActive(pointId);
            parseRoutes($(".point[data-routeId='" + pointId + "']"));

            setTimeout(function () {
              whiteMap();
              actualRouteCard(pointId);

              if ($(window).width() < 500) {
                checkLowestPath();
              }
            }, 400);
          }
        }
      }

    });

    // Глоу по наведению

    $('.route__container').mouseenter(function () {

      $parent = $(this).parents('.routes__row');
      $children = $(this).children('.routes__line');

      if ($(this).hasClass('active') == true) {

        $children.addClass('active');
        addGlow($children);
      }
    }).mouseleave(function () {

      $children = $(this).children('.routes__line');

      $children.removeClass('active');

      routesGlow.removeAll();
    });

    // Глоу при наведении на карте на маршрут

    myGeoObjects.events
      .add('mouseenter', function (e) {

        if (e.get('target').options.get('objectType') == 'route') {

          console.log();

          var lineId = e.get('target').options.get('routeId');

          var $line = $(document).find('.routes__container.active').find('.routes__row.active').find(".routes__line[data-lineid='" + lineId + "']");

          console.log();

          if ($line.length > 0) {

            addGlow($line);

          }
        }
      })

      .add('mouseleave', function (e) {

        routesGlow.removeAll();

      });

    // Закрываем карточку

    $('.routes__title').click(function () {

      myMap.setCenter([55.625857, 37.661209]);
      myMap.setZoom(11);

      makeShieldUnactive();
      removeRoutes();
      normalMap();
      routesGlow.removeAll();
      returnAdditionalPoints();

      $('.routes__list').removeClass('active');
      $(".route__container").removeClass('active').removeClass('disable');
      $('.accessibility__header').removeClass('unactive');
      $('.accessibility-list').removeClass('active');
    });
  };
}