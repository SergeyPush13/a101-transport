require('./index.scss');

require("jquery-mousewheel")($);
require('malihu-custom-scrollbar-plugin')($);
require('slick-carousel');

const location = require('./js/location');
import accessibilityTabs from'./js/accessibility-tabs';

import infraMap from './js/infra-map';
const infraList = require('./js/infra-list');

import accessibilityNews from './js/accessibility-news';

$(document).ready(function () {
  location();
  accessibilityTabs();
  infraMap();
  infraList();
  accessibilityNews();
})

if (module.hot) {
  module.hot.accept();
}