const path = require('path');
const webpack = require("webpack");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    main: './index.js',
  },
  output: {
    path: path.resolve(__dirname, 'assets'),
    filename: 'js/main.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          "babel-loader",
        ],
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          //resolve-url-loader may be chained before sass-loader if necessary
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: function () {
                  return [
                    require('autoprefixer'),
                    require('postcss-flexbugs-fixes')
                  ];
                }
              }
            },
            'sass-loader'
          ]
        })
      },
      {
        test: /(\.jpg|\.png|\.ico|.gif|\.tif|\.svg)\??.*$/,
        use: 'url-loader?limit=30000&name=images/[name].[ext]?v=[hash]'
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: 'file-loader?name=fonts/[name].[ext]?v=[hash]'
      },
      {test: /jquery-mousewheel/, loader: "imports-loader?define=>false&this=>window"},
      {test: /malihu-custom-scrollbar-plugin/, loader: "imports-loader?define=>false&this=>window"},
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'css/styles.css',
      allChunks: true,
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 8000,
      proxy: 'http://a101.transport.com/',
      open: false,
    })
  ],
  devServer: {
    proxy: {
      "*": {
        target: "http://localhost:8000",
        changeOrigin: true,
        secure: false,
      }
    }
  }
};
